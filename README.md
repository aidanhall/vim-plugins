# Vim Plugins
I thought it would be good to make a fairly portable Vim configuration.

This collection uses Vim's built-in package support.
I've put ones that may as well always be loaded in `start`,
but have kept ones that could cause issues (notably of portability)
in `opt`, from where they may still be loaded manually.

## Usage

```shell
$ mkdir -p ~/.vim/after/pack
$ cd ~/.vim/after/pack
$ git clone https://gitlab.com/aidanhall/vim-plugins.git
```

If you want an optional package, for example auto-pairs:
```vim
packadd auto-pairs
```
